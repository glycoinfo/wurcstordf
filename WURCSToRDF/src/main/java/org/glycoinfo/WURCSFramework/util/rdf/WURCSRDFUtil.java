package org.glycoinfo.WURCSFramework.util.rdf;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

public class WURCSRDFUtil {

	/**
	 * Convert to URI string
	 * @param a_strUrl is a target String
	 * @return encoded String
	 */
	public static String getURLString(String a_strData){

		String t_strURL = "";
		try {
			t_strURL = URLEncoder.encode(a_strData, "utf-8");
			t_strURL = t_strURL.replaceAll("<", "%3C");
			t_strURL = t_strURL.replaceAll(">", "%3E");
			t_strURL = t_strURL.replaceAll("\\*", "%2A");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return t_strURL;
	}

	private static String getString(Object a_oData) {
		if(a_oData instanceof  String)
			return (String)a_oData;
		if( a_oData instanceof Integer)
			return String.valueOf((Integer)a_oData);
		if( a_oData instanceof Double)
			return String.valueOf((Double)a_oData);
		if( a_oData instanceof Boolean)
			return String.valueOf((Boolean)a_oData);
		if( a_oData instanceof Character)
			return String.valueOf((Character)a_oData);
		if( a_oData instanceof URL)
			return ((URL)a_oData).toString();
		return null;
	}

	public static String getLiteral(Object a_oData) {
		String t_strLiteral = getString(a_oData);

		return t_strLiteral;
	}
}
