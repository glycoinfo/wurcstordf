package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

import java.net.URL;

import org.glycoinfo.WURCSFramework.util.WURCSStringUtils;

/**
 * Class for generation of URI related Glycoinfo RDF
 * @author MasaakiMatsubara
 *
 */
public class GlycoinfoURI {

	/** http://rdf.glycoinfo.org/component/{cardinality}_{WURCS_MS} */
	public static String getComponent ( String a_strComponent ) { return concatURI(BaseURI.COMPONENT, a_strComponent); };

	/** http://rdf.glycoinfo.org/monosaccharide/alias/wurcs/{MS_WURCS} */
	public static String getMSAliasWURCS        ( String a_strMSWURCS )    { return concatURI(BaseURI.MS_ALIAS_WURCS,         a_strMSWURCS); };
	/** http://rdf.glycoinfo.org/monosaccharide/alias/wurcs/{MS_CarbBank} */
	public static String getMSAliasCarbBank     ( String a_strMSCarbBank ) { return concatURI(BaseURI.MS_ALIAS_CARBBANK,      a_strMSCarbBank); };
	/** http://rdf.glycoinfo.org/monosaccharide/alias/wurcs/{IUPAC_Condensed_MS} */
	public static String getMSAliasIUPACCondens ( String a_strMSIUPAC )    { return concatURI(BaseURI.MS_ALIAS_IUPAC_CONDENS, a_strMSIUPAC); };


	protected static String concatURI(String a_strBaseURI, Object a_oVar) {
		return a_strBaseURI+"/"+encodeToURL(convertToString(a_oVar));
	}

	protected static String encodeToURL(String str) {
		return  WURCSStringUtils.getURLString(str);
	}

	protected static String convertToString(Object obj) {
		if(obj instanceof  String){
			return (String)obj;
		}else if( obj instanceof Integer){
			return String.valueOf((Integer)obj);
		}else if( obj instanceof Double){
			return String.valueOf((Double)obj);
		}else if( obj instanceof Boolean){
			return String.valueOf((Boolean)obj);
		}else if( obj instanceof Character){
			return String.valueOf((Character)obj);
		}else if( obj instanceof URL){
			return ((URL)obj).toString();
		}
		return null;
	}

}
