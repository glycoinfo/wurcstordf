package org.glycoinfo.WURCSFramework.wurcs.rdf;

import org.glycoinfo.WURCSFramework.util.rdf.WURCSRDFUtil;
import org.glycoinfo.WURCSFramework.wurcs.owl.WURCSDataProperty;

public class WURCSTripleData extends WURCSTriple {

	private Object m_oObject;

	public WURCSTripleData(WURCSComponent a_oS, WURCSDataProperty a_oP, Object a_oO) throws RDFException {
		super(a_oS, a_oP);
		this.m_oObject = a_oO;

		if ( a_oP.getObjectClassName() != null && !a_oO.getClass().getName().equals( a_oP.getObjectClassName() ) )
			throw new RDFException("Object datatype and predicate range are not match.");
	}

	public String getObjectString() {
		return WURCSRDFUtil.getLiteral(this.m_oObject);
	}
}
