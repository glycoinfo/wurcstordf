package org.glycoinfo.WURCSFramework.wurcs.rdf;

import org.glycoinfo.WURCSFramework.wurcs.owl.PropertyInterface;

public abstract class WURCSTriple {

	private WURCSComponent m_oSubject;
	private PropertyInterface m_oPredicate;

	public WURCSTriple(WURCSComponent a_oS, PropertyInterface a_oP) throws RDFException {
		this.m_oSubject = a_oS;
		this.m_oPredicate = a_oP;

		if ( a_oS.getWURCSClass() != a_oP.getSubjectClass() )
			throw new RDFException("Subject class and predicate domain are not match.");
	}

	public WURCSComponent getSubject() {
		return this.m_oSubject;
	}

	public PropertyInterface getPredicate() {
		return this.m_oPredicate;
	}
}
