package org.glycoinfo.WURCSFramework.wurcs.rdf;

/**
 * Class for exception of RDF
 * @author MasaakiMatsubara
 *
 */
public class RDFException extends Exception {

	public RDFException(String a_strMessage) {
		super(a_strMessage);
	}

	public RDFException(String a_strMessage,Throwable a_objThrowable) {
		super(a_strMessage,a_objThrowable);
	}

}
