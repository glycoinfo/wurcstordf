package org.glycoinfo.WURCSFramework.wurcs.owl;

public enum WURCSClass implements ClassInterface {

	WURCS      ("WURCS"),	// SubclassOf GLYCORDF.A_GSEQ
	// Section
	COUNT_SECT ("Count_section"),
	URES_SECT  ("UniqueRES_section"),
	RESSEQ_SECT("RESSequence_section"),
	LIN_SECT   ("LIN_section"),
	// Residue component
	URES       ("UniqueRES"),
	RES        ("RES"),
	// Linkage component
	LIN        ("LIN"),
	GLIPS      ("GLIPS"),
	GLIP       ("GLIP"),
	// Monosaccharide
	MS         ("Monosaccharide"),
	// Modification
	MOD        ("MOD"),
	LIPS       ("LIPS"),
	LIP        ("LIP"),
	MAP        ("MAP"),
	// Value
	URES_NUM   ("UniqueRES_number"),
	RES_ID     ("RES_index"),
	POSITION   ("Position"),
	DIRECTION  ("Direction"),
	MAP_NUM    ("MAP_star_number"),
	// Monosaccharide component
	BACKBONE   ("Backbone"),
	SKELETON   ("SkeletonCode"),
	B_CARBON   ("BackboneCarbon"),
	C_DESC     ("CarbonDescriptor"),
	ANOMER     ("Anomer"),
	ANOM_SYMBOL("Anomeric_symbol"),
	// Ambiguous parameter
//	PROB       ("Probability"),
	B_PROB     ("Backbone_probability"),		// Subclass of PROB
	M_PROB     ("Modification_probability"),	// Subclass of PROB
	PROB_VAL   ("Probability_value"),
	REP        ("Repeat"),
	REP_COUNT  ("Repeat_count");

	private final Ontology m_enumOntology = Ontology.WURCS;
	private String m_strName;

	private WURCSClass(String a_strName) {
		this.m_strName = a_strName;
	}

	public Ontology getOntology() {
		return this.m_enumOntology;
	}

	public String getName() {
		return this.m_strName;
	}

	public String getURI() {
		return this.m_enumOntology.getNameSpace()+this.m_strName;
	}
}
