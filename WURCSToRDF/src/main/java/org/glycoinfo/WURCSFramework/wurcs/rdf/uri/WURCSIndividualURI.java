package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

public interface WURCSIndividualURI {

	public String getNS();
	public String getURIFragment();
	public String getURI();
}
