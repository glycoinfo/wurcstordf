package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

import org.glycoinfo.WURCSFramework.wurcs.graph.AnomericSymbol;
import org.glycoinfo.WURCSFramework.wurcs.owl.WURCSOWL;

public enum AnomericSymbolURI implements WURCSIndividualURI {
	ALPHA  (AnomericSymbol.ALPHA,   ""),
	BETA   (AnomericSymbol.BETA,    ""),
	UP     (AnomericSymbol.UP,      ""),
	DOWN   (AnomericSymbol.DOWN,    ""),
	UNKNOWN(AnomericSymbol.UNKNOWN, "");

	private AnomericSymbol m_enumAnomSymbol;
	private String m_strURIFragment;

	private AnomericSymbolURI(AnomericSymbol a_enumAnom, String a_strFragment) {
		this.m_enumAnomSymbol = a_enumAnom;
		this.m_strURIFragment = a_strFragment;
	}

	public String getNS() {
		return WURCSOWL.NS;
	}

	public String getURIFragment() {
		return this.m_strURIFragment;
	}

	public String getURI() {
		return getNS()+getURIFragment();
	}

	public AnomericSymbolURI forCharactor( char a_cSymbol ) {
		AnomericSymbol t_enumAnom = AnomericSymbol.forChar(a_cSymbol);
		for ( AnomericSymbolURI t_enumAnomURI : AnomericSymbolURI.values() ) {
			if ( t_enumAnomURI.m_enumAnomSymbol != t_enumAnom ) continue;
			return t_enumAnomURI;
		}
		return null;
	}
}
