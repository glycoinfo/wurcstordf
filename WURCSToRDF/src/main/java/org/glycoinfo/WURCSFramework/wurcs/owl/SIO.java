package org.glycoinfo.WURCSFramework.wurcs.owl;

public class SIO {
	public static final String PREFIX = "glycan";
	public static final String NS = "http://semanticscience.org/resource/";
	public static String getHashURI(String a_strFragment) {return NS+a_strFragment;}

	// Property
	public static final String HAS_VAL	= NS+"SIO_000300"; // has_value
}
