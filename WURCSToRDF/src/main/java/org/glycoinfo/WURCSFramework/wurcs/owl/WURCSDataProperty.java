package org.glycoinfo.WURCSFramework.wurcs.owl;

public enum WURCSDataProperty implements PropertyInterface {

	HAS_URES_COUNT ("has_UniqueRES_count", WURCSClass.COUNT_SECT, (new Integer(0)).getClass().getName()),
	HAS_RES_COUNT  ("has_RES_count"      , WURCSClass.COUNT_SECT, (new Integer(0)).getClass().getName()),
	HAS_LIN_COUNT  ("has_LIN_count"      , WURCSClass.COUNT_SECT, (new Integer(0)).getClass().getName());

	private final Ontology m_enumOntology = Ontology.WURCS;
	private String m_strName;
	private WURCSClass m_enumSubjectClass;
	private String m_strObjectClassName;

	private WURCSDataProperty(String a_strName, WURCSClass a_enumSClass, String a_strClass ) {
		this.m_strName = a_strName;
		this.m_enumSubjectClass = a_enumSClass;
		this.m_strObjectClassName = a_strClass;
	}

	public Ontology getOntology() {
		return this.m_enumOntology;
	}

	public String getName() {
		return this.m_strName;
	}

	public String getURI() {
		return this.m_enumOntology.getNameSpace()+this.m_strName;
	}

	public ClassInterface getSubjectClass() {
		return this.m_enumSubjectClass;
	}

	public String getObjectClassName() {
		return this.m_strObjectClassName;
	}
}
