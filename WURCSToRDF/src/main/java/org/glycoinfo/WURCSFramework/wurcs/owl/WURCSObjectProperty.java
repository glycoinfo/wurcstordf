package org.glycoinfo.WURCSFramework.wurcs.owl;

public enum WURCSObjectProperty implements PropertyInterface {

	HAS_WURCS      ("has_WURCS",                    null,                   WURCSClass.WURCS      ),
	// WURCS section
	HAS_COUNT_SECT ("has_count_section",            WURCSClass.WURCS,       WURCSClass.COUNT_SECT ),
	HAS_URES_SECT  ("has_UniqueRES_section",        WURCSClass.WURCS,       WURCSClass.URES_SECT  ),
	HAS_RESSEQ_SECT("has_RES_sequence_section",     WURCSClass.WURCS,       WURCSClass.RESSEQ_SECT),
	HAS_LIN_SECT   ("has_LIN_section",              WURCSClass.WURCS,       WURCSClass.LIN_SECT   ),
	// URES component
	HAS_URES       ("has_UniqueRES",                WURCSClass.URES_SECT,   WURCSClass.URES       ),
	IS_M           ("is_Monosaccharide",            WURCSClass.URES,        WURCSClass.MS         ),
	HAS_URES_NUM   ("has_UniqueRES_number",         WURCSClass.URES,        WURCSClass.URES_NUM   ),
	// RES component
	HAS_RES        ("has_RES",                      WURCSClass.RESSEQ_SECT, WURCSClass.RES        ),
	HAS_RES_ID     ("has_RES_index",                WURCSClass.RES,         WURCSClass.RES_ID     ),
	// Monosaccharide component
	HAS_BACKBONE   ("has_Backbone",                 WURCSClass.MS,          WURCSClass.BACKBONE   ),
	HAS_SC         ("has_SkeletonCode",             WURCSClass.BACKBONE,    WURCSClass.SKELETON   ),
	HAS_CARBON     ("has_BackboneCarbon",           WURCSClass.SKELETON,    WURCSClass.B_CARBON   ),
	HAS_POS_BC     ("has_position",                 WURCSClass.B_CARBON,    WURCSClass.POSITION   ), // same as HAS_POS
	HAS_ANOM_POS   ("has_anomeric_position",        WURCSClass.ANOMER,      WURCSClass.POSITION   ),
	HAS_ANOM_SYMBOL("has_anomeric_symbol",          WURCSClass.ANOMER,      WURCSClass.ANOM_SYMBOL),
	HAS_MOD        ("has_MOD",                      WURCSClass.MS,          WURCSClass.MOD        ),
	HAS_MAP        ("has_MAP",                      WURCSClass.MOD,         WURCSClass.MAP        ),
	HAS_LIPS       ("has_LIPS",                     WURCSClass.MOD,         WURCSClass.LIPS       ),
	HAS_LIP        ("has_LIP",                      WURCSClass.LIPS,        WURCSClass.LIP        ),
	HAS_POS        ("has_position",                 WURCSClass.LIP,         WURCSClass.POSITION   ),
	HAS_DIRECTION  ("has_direction",                WURCSClass.LIP,         WURCSClass.DIRECTION  ),
	HAS_MAP_NUM    ("has_MAP_star_number",          WURCSClass.LIP,         WURCSClass.MAP_NUM    ),
	// Linkage component
	HAS_LIN        ("has_LIN",                      WURCSClass.LIN_SECT,    WURCSClass.LIN        ),
	HAS_MAP_G      ("has_MAP",                      WURCSClass.LIN,         WURCSClass.MAP        ), // same as HAS_MAP
	HAS_GLIPS      ("has_GLIPS",                    WURCSClass.LIN,         WURCSClass.GLIPS      ),
	HAS_GLIP       ("has_GLIP",                     WURCSClass.GLIPS,       WURCSClass.GLIP       ),
	HAS_POS_G      ("has_position",                 WURCSClass.GLIP,        WURCSClass.POSITION   ), // same as HAS_POS
	HAS_DIRECTION_G("has_direction",                WURCSClass.GLIP,        WURCSClass.DIRECTION  ), // same as HAS_DIRECTION
	HAS_MAP_NUM_G  ("has_MAP_star_number",          WURCSClass.GLIP,        WURCSClass.MAP_NUM    ), // same as HAS_MAP_NUM
	// Ambiguous component
	HAS_REP        ("has_repeat",                   WURCSClass.LIN,         WURCSClass.REP        ),
	HAS_REP_MAX    ("has_repeat_count_max",         WURCSClass.REP,         WURCSClass.REP_COUNT  ),
	HAS_REP_MIN    ("has_repeat_count_min",         WURCSClass.REP,         WURCSClass.REP_COUNT  ),
	HAS_PROB_B     ("has_probability",              WURCSClass.LIP,         WURCSClass.B_PROB     ),
	HAS_PROB_M     ("has_probability",              WURCSClass.LIP,         WURCSClass.M_PROB     ), // same as HAS_PROB_B
	HAS_PROB_B_G   ("has_probability",              WURCSClass.GLIP,        WURCSClass.B_PROB     ), // same as HAS_PROB_B
	HAS_PROB_M_G   ("has_probability",              WURCSClass.GLIP,        WURCSClass.M_PROB     ), // same as HAS_PROB_M
	HAS_PROB_LOW_B ("has_probability_lower",        WURCSClass.B_PROB,      WURCSClass.PROB_VAL   ),
	HAS_PROB_LOW_M ("has_probability_lower",        WURCSClass.M_PROB,      WURCSClass.PROB_VAL   ), // same as HAS_PROB_LOW_B
	HAS_PROB_UP_B  ("has_probability_upper",        WURCSClass.B_PROB,      WURCSClass.PROB_VAL   ),
	HAS_PROB_UP_M  ("has_probability_upper",        WURCSClass.M_PROB,      WURCSClass.PROB_VAL   ); // same as HAS_PROB_UP_B

	private final Ontology m_enumOntology = Ontology.WURCS;
	private String m_strName;
	private WURCSClass m_enumSubjectClass;
	private WURCSClass m_enumObjectClass;

	private WURCSObjectProperty(String a_strName, WURCSClass a_enumSClass, WURCSClass a_enumOClass) {
		this.m_strName = a_strName;
		this.m_enumSubjectClass = a_enumSClass;
		this.m_enumObjectClass = a_enumOClass;
	}
	public Ontology getOntology() {
		return this.m_enumOntology;
	}

	public String getName() {
		return this.m_strName;
	}

	public String getURI() {
		return this.m_enumOntology.getNameSpace()+this.m_strName;
	}

	public ClassInterface getSubjectClass() {
		return this.m_enumSubjectClass;
	}

	public ClassInterface getObjectClass() {
		return this.m_enumObjectClass;
	}

}
