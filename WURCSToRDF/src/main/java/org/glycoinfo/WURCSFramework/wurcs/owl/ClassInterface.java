package org.glycoinfo.WURCSFramework.wurcs.owl;

public interface ClassInterface {

	public Ontology getOntology();
	public String getName();
	public String getURI();

}
