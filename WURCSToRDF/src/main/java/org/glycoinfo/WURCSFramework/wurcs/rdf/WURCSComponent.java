package org.glycoinfo.WURCSFramework.wurcs.rdf;

import org.glycoinfo.WURCSFramework.wurcs.owl.WURCSClass;

public class WURCSComponent {

	private WURCSClass m_enumClass;
	private String m_strValue;

	public WURCSComponent(WURCSClass a_enumClass, String a_strValue ) {
		this.m_enumClass = a_enumClass;
		this.m_strValue = a_strValue;
	}

	public WURCSClass getWURCSClass() {
		return this.m_enumClass;
	}

	public String getURI() {
		return null;
	}
}
