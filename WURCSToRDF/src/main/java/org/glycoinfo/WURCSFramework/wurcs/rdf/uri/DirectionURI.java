package org.glycoinfo.WURCSFramework.wurcs.rdf.uri;

public enum DirectionURI implements WURCSIndividualURI {
	;

	public String getNS() {
		return null;
	}

	public String getURIFragment() {
		return null;
	}

	public String getURI() {
		return getNS()+getURIFragment();
	}

}
