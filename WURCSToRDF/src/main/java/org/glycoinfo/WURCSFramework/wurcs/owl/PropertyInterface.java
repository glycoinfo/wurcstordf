package org.glycoinfo.WURCSFramework.wurcs.owl;

public interface PropertyInterface extends ClassInterface {

	public ClassInterface getSubjectClass();
}
