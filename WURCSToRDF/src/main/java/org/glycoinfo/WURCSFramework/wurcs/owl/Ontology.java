package org.glycoinfo.WURCSFramework.wurcs.owl;


public enum Ontology {

	/*
	 * @prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
	 * @prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
	 * @prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
	 * @prefix owl:   <http://www.w3.org/2002/07/owl#> .
	 * @prefix glycan: <http://purl.jp/bio/12/glyco/glycan#> .
	 * @prefix glytoucan: <http://www.glytoucan.org/glyco/owl/glytoucan#> .
	 * @prefix wurcs: <http://www.glycoinfo.org/glyco/owl/wurcs#> .
	 * @prefix dcterms: <http://purl.org/dc/terms/> .
	 */
	RDFS     ("rdfs",      "http://www.w3.org/2000/01/rdf-schema#"),
	XSD      ("xsd",       "http://www.w3.org/2001/XMLSchema#"),
	RDF      ("rdf",       "http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
	OWL      ("owl",       "http://www.w3.org/2002/07/owl#"),
	GLYCAN   ("glycan",    "http://purl.jp/bio/12/glyco/glycan#"),
	GLYTOUCAN("glytoucan", "http://www.glytoucan.org/glyco/owl/glytoucan#"),
	WURCS    ("wurcs",     "http://www.glycoinfo.org/glyco/owl/wurcs#"),
	DCTERM   ("dcterms",   "http://purl.org/dc/terms/");

	private String m_strPrefix;
	private String m_strNS;

	private Ontology(String a_strPrefix, String a_strPrefixURI) {
		this.m_strPrefix    = a_strPrefix;
		this.m_strNS = a_strPrefixURI;
	}

	public String getPrefix() {
		return this.m_strPrefix;
	}

	public String getNameSpace() {
		return this.m_strNS;
	}

	public String getPrefixScheme() {
		return "@prefix "+this.m_strPrefix+": <"+this.m_strNS+"> . \n";
	}

}
