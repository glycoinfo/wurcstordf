package org.glycoinfo.WURCSFramework.wurcs.rdf;

import org.glycoinfo.WURCSFramework.wurcs.owl.WURCSObjectProperty;

public class WURCSTripleObject extends WURCSTriple {

	private WURCSComponent m_oObject;

	public WURCSTripleObject(WURCSComponent a_oS, WURCSObjectProperty a_oP, WURCSComponent a_oO) throws RDFException {
		super(a_oS, a_oP);
		this.m_oObject = a_oO;

		if ( a_oO.getWURCSClass() != a_oP.getObjectClass() )
			throw new RDFException("Object class and predicate domain are not match.");
	}

	public WURCSComponent getObject() {
		return this.m_oObject;
	}

}
